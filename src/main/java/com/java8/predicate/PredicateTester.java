package com.java8.predicate;

import java.util.Arrays;
import java.util.List;
import java.util.function.IntPredicate;

public class PredicateTester {

    static IntPredicate evenPredicate = new IntPredicate() {
        @Override
        public boolean test(int value) {
            return value % 2 == 0;
        }
    };

    public static void main(String[] args) {

        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 12, 14, 15, 23, 45, 76);

        System.out.println("Print All Numbers:");
        print(list, n -> true);

        System.out.println("Print Even Numbers:");
        print(list, evenPredicate);

        System.out.println("Print Odd Numbers:");
        print(list, n -> n % 2 != 0);
    }

    static public void print(List<Integer> list, IntPredicate predicate) {
        for (Integer i : list) {
            if (predicate.test(i)) {
                System.out.print(i + ", ");
            }
        }
        System.out.println();
    }

}
