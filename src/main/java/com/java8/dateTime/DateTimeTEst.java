package com.java8.dateTime;

import java.time.LocalDateTime;
import java.time.Month;

public class DateTimeTEst {

    public static void main(String[] args) {

        LocalDateTime localDateTime = LocalDateTime.now();
        System.out.println(localDateTime);
        System.out.println(localDateTime.getDayOfMonth());
        System.out.println(localDateTime.toLocalDate());
        System.out.println(localDateTime.getSecond());
        System.out.println(LocalDateTime.of(1989, Month.APRIL, 5, 12, 12));
    }
}
