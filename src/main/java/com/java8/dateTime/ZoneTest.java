package com.java8.dateTime;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZonedDateTime;

public class ZoneTest {

    public static void main(String[] args) {
        System.out.println(ZoneId.systemDefault());
        System.out.println(ZoneId.SHORT_IDS.get("ECT"));
        System.out.println(ZonedDateTime.of(LocalDateTime.now(),ZoneId.of(ZoneId.SHORT_IDS.get("ECT"))));
    }
}
