package com.java8.deafults;

public class DefaultMethod {
    public static void main(String[] args) {
        Car car = new Car();
        Vehicle vehicle = new Car();
        FourWheeler fourWheeler = new Car();
        car.run();
        vehicle.run();
        fourWheeler.run();
        Vehicle.start();
    }
}

interface Vehicle {
    static void start() {
        System.out.println(Vehicle.class.getSimpleName() + " - Static: start the vehicle");
    }

    default void run() {
        System.out.println(Vehicle.class.getSimpleName() + " - running");
    }
}

interface FourWheeler {
    default void run() {
        System.out.println(FourWheeler.class.getSimpleName() + " - running");
    }
}

class Car implements Vehicle, FourWheeler {
    @Override
    public void run() {
        System.out.println(Vehicle.super.getClass());
        Vehicle.super.run();
        System.out.println(FourWheeler.super.getClass());
        FourWheeler.super.run();
        System.out.println(Car.class.getSimpleName() + " - running");
    }
}