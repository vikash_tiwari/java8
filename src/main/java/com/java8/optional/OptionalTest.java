package com.java8.optional;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class OptionalTest {

    public static void main(String[] args) {
        Optional nullOptional = Optional.ofNullable(null);
        Optional<Integer> intOptional = Optional.of(1234);
        Optional nullOptional2 = Optional.ofNullable(563);
        List<Optional> optionals = Arrays.asList(nullOptional, nullOptional2, intOptional);
        nullOptional2.ifPresent(System.out::println);
        nullOptional.ifPresent(n -> System.out.println(n));
        intOptional.ifPresent(n -> System.out.println(n));
        //sum
        System.out.println(intOptional.get() + (Integer) nullOptional.orElse(0) + (Integer) nullOptional2.orElse(0));
    }

}
