package com.java8.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

public class StreamTest {
    public static void main(String[] args) {
        //for each
        Random random = new Random();
//        random.ints().limit(20).forEach(System.out::println);

        //map
        List<Integer> list = Arrays.asList(1, 2, 4, 3, 5, 8, 6, 7);
        list.stream().map(n -> n * 10).filter(n -> n % 3 == 0).sorted().forEach(System.out::println);
        System.out.println(list.stream().map(n -> n * 10).filter(n -> n % 20 == 0).sorted().collect(Collectors.toSet()).getClass());
        System.out.println(list.stream().map(n -> n * 10).filter(n -> n % 20 == 0).sorted().mapToInt(n -> n).summaryStatistics().getMax());
        List<String> names = Arrays.asList("vic", "nic", "ric", "raymond");
        System.out.println(names.stream().map(name -> name + " - shelton").filter(n -> !n.isEmpty()).sorted().collect(Collectors.joining(",")));
        System.out.println(names.stream().filter(n -> n.length()==3).count());
    }
}
