package com.java8.lambda;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class SortCollection {

    public static void main(String[] args) {
        List<Integer> list = new ArrayList();
        list.add(23);
        list.add(53);
        list.add(523);
        Collections.sort(list, (n1, n2) -> n1.compareTo(n2));
        list.forEach(System.out::println);
        String name = "Vikash Tiwari";
        Test test = new Test();
        test.sayHello(name);
        test.sayHi(name);
        MathOperation sum = (a, b) -> a + b;
        MathOperation subtraction = (a, b) -> a - b;
        MathOperation product = (a, b) -> a * b;
        System.out.println(sum.operation(3, 4));
        System.out.println(subtraction.operation(3, 4));
        System.out.println(product.operation(3, 4));
    }
}

class Test implements GreetingService {

    @Override
    public void saySomething(String name) {
        System.out.println("say something " + name);
    }
}

interface MathOperation {
    public int operation(int a, int b);
}

interface GreetingService {
    default public void sayHello(String name) {
        System.out.println("Hello " + name);
    }

    default public void sayHi(String name) {
        System.out.println("Hi! " + name);
    }

    public void saySomething(String name);
}
